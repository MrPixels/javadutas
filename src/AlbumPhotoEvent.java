import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Classe de gestion d'album photo Evenementiel
 * @version 1.0
 */
public class AlbumPhotoEvent {
	
	
	/**
	 * Nom de l'album Photo
	 */
	private String nom;
	
	/**
	 * Num de l'album Photo
	 */
	private int num;
	
	/**
	 * liste regroupant les photos Evenementiel
	 */
	private Map<String,PhotoEvent> photosEvent;
	
	/**
	 * Evenelent auquel est lier l'album
	 */
	private Event event;
	
	/**
	 * Construit un album PhotoEvent
	 * @param event
	 */
	public AlbumPhotoEvent(Event event){
		this.num = 0;
		this.event =  event;
		this.nom = event.getNom();
		this.photosEvent = new HashMap<String, PhotoEvent>();
	}
	
	/**
	 * Construit l'album avec un fichier
	 * @param nomFichier
	 * @param nom
	 * @throws FileNotFoundException
	 */
	public AlbumPhotoEvent(String nomFichier,String nom,Event event) throws FileNotFoundException{
		this.num = 0;
		this.event = event;
		this.nom = nom;
		this.charger(nomFichier);
	}
	
	/**
	 * Ajouter une photo a l'album Photo
	 * @param path - chemin d'acces à la photo
	 * @throws Exception
	 */
	public void ajoutPhoto(String path) throws Exception{
		PhotoEvent p = new PhotoEvent(path,this.event);
		this.photosEvent.put(p.getNom(),p);
	}
	
	/**
	 * Chercher un photo sur l'album
	 * @param nom de la photo a chercher
	 * @return la photo correspondante
	 */
	public PhotoEvent chercherPhotoEve(String nom) {
		return this.photosEvent.get(nom);
	}
	
	/**
	 * Retourne le nom de l'album
	 * @return nom de l'album
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Retourne le num de l'album
	 * @return num de l'album
	 */
	public int getNum() {
		return this.num;
	}
	
	/**
	 * Change le num�ro de l'album
	 */
	public void setNum(int num) {
		this.num = num;
	}
	
	/**
	 * Retourne une String representant l'album
	 * @return une string representant l'album 
	 */
	public String toString(){
		String s = new String();
		s = "AlbumEvent:"+this.getNom()+"\n";
		for(Personne p : this.event.getListePersonne().values()) {
			s+=p.toString()+"\n";
		}
		for(PhotoEvent currentValue : photosEvent.values()){
			s+=currentValue.toString()+"\n";
		}
		return s;
	}
	
	/**
	 * Renvoie une string pour l'ecriture sur fichier
	 * @return String pour ecriture sur fichier
	 */
	public String toStringFichier() {
		String s = new String();
		for(PhotoEvent currentValue : photosEvent.values()){
			s+=currentValue.toString()+"\n";
		}
		return s;
	}
	
	/**
	 * Sauvegarde l'album sur un fichier
	 * @param nomFichier - nom du fichier dans lequel registrer l'album
	 */
	public void sauvegarde(){
		BufferedWriter bOut = null;
		PrintWriter pOut = null;
		try{
			File inputFile = new File("AlbumEvent_"+this.getNom()+".txt");
			bOut = new BufferedWriter(new FileWriter(inputFile)) ;
			pOut = new PrintWriter(bOut) ;
			pOut.println(this.toStringFichier());
		}
		catch(FileNotFoundException e) { 
			System.out.println(e) ;
        }catch(IOException e) { 	
        	System.out.println(e) ;
		}catch(Exception e) { 
			System.out.println(e) ;
		}finally{
			if (pOut != null) {
				pOut.close();
			}
		}
	}
	
	/**
	 * Creer un album grace a un fichier 
	 * @param nomFichier
	 * @throws FileNotFoundException
	 */
	public void charger(String nomFichier) throws FileNotFoundException{
		File Fichier = new File(nomFichier);
		BufferedReader bIn = null;
		String ligne = null;
		try{
			bIn = new BufferedReader(new FileReader(Fichier));
			ligne = bIn.readLine();
			while (ligne != null)	{
				try {
					ajoutPhoto(ligne);
				}catch(PhotoNotFoundException e){
					System.out.println(e);
				}catch(EventNotFoundException e){
					System.out.println(e);
				}
			ligne = bIn.readLine();
			}
		}catch(IOException e) { 
			System.out.println(e) ;
	    }catch(Exception e){
			System.out.println(e) ;
		}
		finally	{
			if (bIn != null) {
				try{
					bIn.close();
				}catch(IOException ec) { 
					System.out.println(ec) ;
	            }
			}
		}
	}
}

