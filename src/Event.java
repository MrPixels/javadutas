/**
 * Classe de gestion d'evenement
 * @version 1.0
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Event {
	
	/**
	 * Liste des personnes presentes a l'evenement
	 */
	private Map<String,Personne> personnes;
	
	/**
	 * Nom de l'evenement
	 */
	private String nomEvent;

	/**
	 * Creation de l'evenement
	 * @param nom evenement
	 */
	public Event(String nom){
		this.nomEvent = nom;
		this.personnes = new HashMap<String,Personne>();
	}
	
	/**
	 * Creer un event grace a un fichier
	 * @param nomFichier
	 * @param nom
	 * @throws FileNotFoundException
	 */
	public Event(String nomFichier,String nom) throws FileNotFoundException{
		this.personnes = new HashMap<String,Personne>();
		this.nomEvent = nom;
		this.charger(nomFichier);
	}
	
	/**
	 * Ajout de personne a l'evenement
	 * @param nom personne
	 * @param mail personne
	 * @throws Exception
	 */
	public void ajouterPersonne(String nom,String mail) {
		try {
			Personne personne = new Personne(nom,mail);
			this.personnes.put(personne.getMail(),personne);
		}
		catch(MailFormatException a){
	        System.out.println(a);
	       	System.out.println("La personne n'a pas pu etre rajoute");
	    }
	}
	
	/**
	 * retourne le nom de l'evenement
	 * @return nom evenement
	 */
	public String getNom() {
		return this.nomEvent;
	}

	/**
	 * Change le nom de l'evenement
	 * @param nom the nom to set
	 */
	public void setNom(String nom){
		this.nomEvent = nom;
	}
	
	/**
	 * Retourne la liste des personnes
	 * @return listPersonne
	 */
	public Map<String,Personne> getListePersonne() {
		return this.personnes;
	}
	
	/**
	 * 
	 */
	public String toString(){
		String s = new String("nom Evenement : "+this.getNom()+"\n");
		for(Personne p : this.personnes.values()){
			s += p.toString()+"\n";
		}
		return s;
		
	}
	
	
	/**
	 * Enleve une personne de la liste des personne presente a l'evenement
	 * @param p
	 * @throws MailFormatException 
	 */
	public void retirerPersonne(String p) {
		try{
			Personne pt = new Personne("",p);
			personnes.remove(pt.getMail());
		} catch(MailFormatException e){
			System.out.println(e);
		}	
	}
	
	/**
	 * Cherche une personne presente dans la liste 
	 * @param nom de la personne a chercher
	 * @throws PersonNotFoundException
	 */
	public Personne chercherPersonne(String nom) throws PersonNotFoundException {
		for(Personne personne : this.personnes.values()){
			if(personne.getNom().equals(nom)){
				return personne;
			}
		}
		throw new PersonNotFoundException();
	}
	
	/**
	 * Renvoie une string pour l'ecriture sur fichier
	 * @return String pour ecriture sur fichier
	 */
	public String toStringFichier(){
		String s = new String();
		for(Personne p : this.personnes.values()){
			s +=p.getNom()+"/"+p.getMail()+",\n"; 
		}
		return s;
	}
	
	/**
	 * sauvegarde l'event sur un fichier
	 */
	public void sauvegarde(){
		BufferedWriter bOut = null;
		PrintWriter pOut = null;
		try{
			File inputFile = new File("Event_"+this.getNom()+".txt");
			bOut = new BufferedWriter(new FileWriter(inputFile)) ;
			pOut = new PrintWriter(bOut) ;
			pOut.println(this.toStringFichier());
		}
		catch(FileNotFoundException e) { 
			System.out.println(e) ;
        }catch(IOException e) { 	
        	System.out.println(e) ;
		}catch(Exception e) { 
			System.out.println(e) ;
		}finally{
			if (pOut != null) {
				pOut.close();
			}
		}
	}
	
	/**
	 * creer un event grace a un fichier
	 * @param nomFichier
	 * @throws FileNotFoundException
	 */
	public void charger(String nomFichier) {
		File Fichier = new File(nomFichier);
		BufferedReader bIn = null;
		String nom = null;
		String mail = null;
		String ligne = null;
		try{
			bIn = new BufferedReader(new FileReader(Fichier));
			ligne = bIn.readLine();
			while (ligne != null){
					StringTokenizer st = new  StringTokenizer(ligne,"/");;
					ligne = st.nextToken();
					nom = ligne;
					ligne = st.nextToken();
					mail = ligne;
					this.ajouterPersonne(nom,mail);
					ligne = bIn.readLine();
			}
		}
		catch(IOException e) { 
			System.out.println(e) ;
	    }catch(Exception e){
			System.out.println(e) ;
		}finally	{
			if (bIn != null) {
				try{
					bIn.close();
				}catch(IOException ec) { 
					System.out.println(ec) ;
	            }
			}
		}
	}
}
