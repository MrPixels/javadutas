/**
 * Classe de gestion d'exception
 * @version 1.0
 */
public class PhotoNotFoundException extends Exception {
	
	/**
	 * Construit un PhotoNotFoundException
	 */
	public PhotoNotFoundException() {
		System.out.println("photo non trouve");
	}
}