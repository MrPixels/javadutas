/**
 * Classe de gestion de personne
 * @version 1.0
 */

public class Personne {
	
	private String nomPersonne;
	private String mailPersonne;
	
	/**
	 * Creation de la personne
	 * @param nom de la personne
	 * @param mail de la personne
	 * @throws Exception
	 */
	public Personne(String nom,String mail) throws MailFormatException  {
		this.nomPersonne = nom;
		if(testMail(mail)){
			this.mailPersonne = mail;
		}
	}
	
	/**
	 * Retourne le nom de la personne
	 * @return nom personne
	 */
	public String getNom() {
		return nomPersonne;
	}

	/**
	 * Retourne le mail de la personne
	 * @return mail Personne
	 */
	public String getMail() {
		return mailPersonne;
	}

	/**
	 * Modifie le Nom de la personne
	 * @param nom pour modification
	 */
	public void setNom(String nom) {
		this.nomPersonne = nom;
	}

	/**
	 * Modification du mail de la personne
	 * @param mail
	 * @throws Exception 
	 */
	public void setMail(String mail) throws MailFormatException {
		if(testMail(mail)){
			this.mailPersonne = mail;
		}
	}
	
	/**
	 * Teste la validiter du mail en tant que mail Personne@Boite-Mail.domaine
	 * @param mail
	 * @return True si le mail est correct 
	 * @throws Exception
	 */
	private boolean testMail(String mail) throws MailFormatException{
		String pattern = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$" ;
		if(mail.matches(pattern)){
			return true;
		} else throw new MailFormatException();
	}
	
	public boolean comparePersonne(Personne P2) {
		return (this.getNom().equals(P2.getNom()) && this.getMail().equals(P2.getMail()));
	}
	
	public String toString(){
		String s = new String(this.getNom()+" "+this.getMail());
		return s;
	}
}

