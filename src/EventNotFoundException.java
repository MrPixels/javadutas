/**
 * Classe de gestion d'exception
 * @version 1.0
 */
public class EventNotFoundException extends Exception {
	
	/**
	 * Construit un EventNotFoundException
	 */
	public EventNotFoundException() {
		System.out.println("Evenement non trouvé");
	}
}
