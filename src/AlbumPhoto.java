/**
 * Classe de gestion d'album photo
 * @version 1.0
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class AlbumPhoto {
	
	/**
	 * Nom de l'album Photo
	 */
	private String nom;
	
	/**
	 * Map regroupant les photos 
	 */
	private Map<String, Photo> photos;
	
	/**
	 * 	Creation de l'album photo
	 * @param nom de l'album photo
	 */
	public AlbumPhoto(String nom){
		this.nom = nom;
		this.photos = new HashMap<String, Photo>();
	}
	
	/**
	 * 	Creation de l'album photo a partir d un fichier
	 * @param fichier a partir duquel l albulm va etre creer
	 * @throws FileNotFoundException 
	 */
	public AlbumPhoto(String path, String nom) throws FileNotFoundException{
		this.nom = nom;
		this.photos = new HashMap<String, Photo>();
		charger(path);
	}
	
	/**
	 * Ajouter une photo � l'album Photo
	 * @param path - chemin d'acces � la photo
	 * @throws PhotoNotFoundException WrongExtensionException  
	 */
	public void ajoutPhoto(String path) throws PhotoNotFoundException,WrongExtensionException {
		Photo p = new Photo(path);
		this.photos.put(p.getNom(),p);
	}
	
	/**
	 * Chercher un photo sur l'album
	 * @param nom de la photo a chercher
	 * @return la photo correspondante
	 */
	public Photo chercherPhoto(String nom){
		return this.photos.get(nom);
	}

	/**
	 * Retourne le nom de l'album
	 * @return nom de l'album
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Modifie le nom de l'album photo
	 * @param nom - nouveau nom de l'album
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Retourne une String representant l'album
	 * @return une string representant l'album 
	 */
	public String toString(){
		String s = new String();
		s = "Album : "+this.getNom()+"\n";
		for(Photo currentValues : photos.values()){
			s+=currentValues.toString()+"\n";
		}
		return s;
	}
	
	public String toFile(){
		String s = new String();
		for(Photo currentValues : photos.values()){
			s+=currentValues.toString()+"\n";
		}
		return s;
	}

	/**
	 * Sauvegarde l'album sur un fichier
	 * @param nomFichier - nom du fichier dans lequel registrer l'album
	 */
	public void sauvegarde(){
		PrintWriter pOut = null;
		try{
			File inputFile = new File("Album_"+this.getNom());
			pOut = new PrintWriter(new BufferedWriter(new FileWriter(inputFile))) ;
			pOut.println(this.toFile());
		}
		catch(FileNotFoundException e) { 
			System.out.println(e) ;
        }catch(IOException e) { 
        	System.out.println(e) ;
		}finally{
			if (pOut != null) {
				pOut.close();
			}
		}
	}
	
	public void charger(String nomFichier) throws FileNotFoundException{
		BufferedReader br = null;
		FileReader fr = null;
		String line;
		try{
			fr = new FileReader(nomFichier);
			br = new BufferedReader(fr);
			while((line = br.readLine()) != null){
				try{
					this.ajoutPhoto(line);
				} catch(PhotoNotFoundException e){
					System.out.println("La photo :"+line+" n a pas pu etre ajouter" + e);
				} catch (WrongExtensionException e) {
					System.out.println("erreur: "+ e);
				}
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e ){
			System.out.println("erreur: "+ e);
		}
		finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("erreur: "+ e);
				}
			}
		}
	}
}
