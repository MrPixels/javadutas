import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TestProjetConsole {
	
	private static Map<String,Event> mapevenement = new HashMap<String,Event>();
	private static Map<String,ArrayList<AlbumPhotoEvent>> listalbumev = new HashMap<String,ArrayList<AlbumPhotoEvent>>();
	private static Map<String,AlbumPhoto> mapalbum = new HashMap<String,AlbumPhoto>();
	
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Bonjour, bienvenue sur le programme de creation et de visualisation d'albums photographiques PIC'VIEW.");
		System.out.println("Il s'offre a vous plusieurs choix :");
		do {
		} while(initMain());
		sc.close();
		System.out.println("Fin Programme");
		
	}
	
	private static boolean initMain(){	
		int isc0;
		
		do{
			System.out.println("*Tapez 1 - Souhaitez-vous modifier un album?");
			System.out.println("*Tapez 2 - Souhaitez-vous modifier un evenement?");
			System.out.println("*Tapez 3 - Lister les personnes d'un evenement?");
			System.out.println("*Tapez 4 - Lister les photographies d'un album?");
			System.out.println("*Taper 5 - Fin programme ");
			
			isc0 = sc.nextInt();
		} while( isc0 <= 0 || isc0 > 5);
		
		switch(isc0){
			case 1:initAlbum();break;
			case 2:initEvent();break;
			case 3:listerPersonnes();break;
			case 4:listerPhoto();break;
			case 5:return false;
		}
		return true;
	}
	
	private static void initAlbum(){	
		int isc1;

		do{
			System.out.println("*Tapez 1 - Souhaitez-vous creer un album?");
			System.out.println("*Tapez 2 - Souhaitez-vous supprimer un album?");
			System.out.println("*Tapez 3 - Souhaitez-vous ajouter une photographie a un album?");
			System.out.println("*Tapez 4 - Souhaitez-vous enlever une photographie a un album?");
			isc1 = sc.nextInt();
		} while( isc1 <= 0 || isc1 > 4);

		
		switch(isc1){
			case 1:creerAlbum();break;
			case 2:supprimerAlbum();break;
			case 3:ajouterPhoto();break;
			case 4:supprimerPhoto();break;
		}
	}

	private static void initEvent(){	
		int isc2;
		
		do{
			System.out.println("*Tapez 1 - Souhaitez-vous creer un evenement?");
			System.out.println("*Tapez 1 - Souhaitez-vous supprimer un evenement?");
			System.out.println("*Tapez 2 - Souhaitez-vous ajouter une personne a un evenement?");
			System.out.println("*Tapez 3 - Souhaitez-vous enlever une personne a un evenement?");
				
			isc2 = sc.nextInt();
		} while( isc2 <= 0 || isc2 > 4);
		
		switch(isc2){
			case 1:creerEvent();break;
			case 2:supprimerEvent();break;
			case 3:ajouterPersonne();break;
			case 4:enleverPersonne();break;
		}
	}
	
	private static void supprimerPhoto() {
		// TODO Auto-generated method stub
		
	}

	private static void ajouterPhoto() {
		// TODO Auto-generated method stub
		
	}

	private static void supprimerAlbum() {
		// TODO Auto-generated method stub
		
	}
	
	private static void supprimerEvent() {
		// TODO Auto-generated method stub
		
	}

	private static void enleverPersonne() {
		String ssc4;
		if(!(mapevenement.isEmpty())){
			for(String currentValue : mapevenement.keySet()){
				System.out.println(currentValue);
			}
			do {
				System.out.println("*Choisir Evenement");
				ssc4 = sc.nextLine();
			} while (!(mapevenement.containsKey(ssc4)));
			Event e = mapevenement.get(ssc4);
			System.out.println(e);
			System.out.println("*Rentrer le mail de la personne a suprimmer");
			ssc4 = sc.nextLine();
			e.retirerPersonne(ssc4);
			System.out.println(e);
		} 
		else { 
			System.out.println("Aucun evenement existant.");
		}
	}

	private static void ajouterPersonne() {
        String ssc7;
        String ssc8;
        if(!(mapevenement.isEmpty())){
        	for(String currentValue : mapevenement.keySet()){
				System.out.println(currentValue);
			}
        	do {
        		System.out.println("*Choisir un evenement dans lequel ajouter une personne");
        		ssc7 = sc.nextLine();
			} while (!(mapevenement.containsKey(ssc7)));
	        Event e = mapevenement.get(ssc7);
	        System.out.println(e);
	        System.out.println("*Nom personne a rajouter");
	        ssc7 = sc.nextLine();
	        System.out.println("*mail personne a rajouter");
	        ssc8 = sc.nextLine();
	        e.ajouterPersonne(ssc7, ssc8);   
        }
        else {
        	System.out.println("Aucun evenement existant.");
        }
    }

	private static void creerEvent(){
        String ssc6;
        Event e;
        for(String currentValue : mapevenement.keySet()){
			System.out.println(currentValue);
		}
        System.out.println("*Nom de l'evenement a creer");
        ssc6 = sc.nextLine();
        if (!(mapevenement.containsKey(ssc6))){
             e = new Event(ssc6);
             mapevenement.put(e.getNom(),e);
             System.out.println("Evenement cree.");
        }
    }

	private static void listerPersonnes(){
		String ssc3;
		
		System.out.println("*Voici la liste des evenements :");
		for(String currentValue : mapevenement.keySet()){
			System.out.println(currentValue);
		}
		System.out.println("*Donnez l'evenement duquel vous souhaitez lister les personnes.\n");
		
		ssc3 = sc.nextLine();
		
		if(mapevenement.containsKey(ssc3)){
			for(Personne currentValue : mapevenement.get(ssc3).getListePersonne().values()){
				System.out.println(currentValue.toString());
			}
		}
	}
	
	private static void listerPhoto(){ 
		int isc4;  
	    String ssc4;
	                
	    System.out.println("Album :");         
	    for(String nom : mapalbum.keySet()){             
	        System.out.println(nom);         
        }
        System.out.println("Album Evenementiel :");     
        for(String nom : listalbumev.keySet()){     
            System.out.println(nom);         
        } 
        do{             
            System.out.println("*1 - Album Evenementiel");             
            System.out.println("*2 - Album Normaux");       
	        isc4 = sc.nextInt();         
	    } while( isc4 <= 0 || isc4 > 2);         
	        
	    switch(isc4){             
	        case 1: do{
		            	System.out.println("*Nom de l'Album ?");     
		                ssc4 = sc.nextLine();
		                if(mapalbum.containsKey(ssc4)){
		                	mapalbum.get(ssc4).toString();
		                }
		                else{
		                    System.out.println("Cet album n'existe pas.");
		                    isc4=0;
		                }
	            	}while(isc4==0);
	                break;    
	                    
	       case 2: System.out.println("*Nom Album ?"); 
	                ssc4 = sc.nextLine();
	                if(listalbumev.containsKey(ssc4)){
	                    System.out.println("La taille de la liste des albums de l'evenement "+ ssc4 +" est : "+listalbumev.get(ssc4).size()); 
	                    System.out.println("De quel album de cet evenement voulez vous obtenir la liste de photographies?");
	                    isc4 = sc.nextInt();
	                    if((isc4<listalbumev.get(ssc4).size()) && (isc4>-1)){
	                        for(AlbumPhotoEvent alb : listalbumev.get(ssc4)){             
	                        	System.out.println(alb.toString());
	                	    }
	                    }
	                    else{
	                        System.out.println("Cet album n'existe pas.");
	                    }
	                }
	                else{
	                	System.out.println("Cet album n'existe pas.");
	                }
	                break;
	     }     
	}

	private static void creerAlbum(){
        int isc5;
        String ssc5;
        
        do{
            System.out.println("Type d'album");
            System.out.println("1 - Album");
            System.out.println("2 - Album Evenementiel");
            isc5 = sc.nextInt();
        }while(isc5 <= 0 || isc5 > 2);
        
        switch(isc5){
            case 1 : System.out.println("nom album :");
                     sc.nextLine(); // passer le backslash n de entr�e
                     ssc5 = sc.nextLine();
                     AlbumPhoto a = new AlbumPhoto(ssc5);
                     mapalbum.put(a.getNom(),a);
                     break;
            case 2 : System.out.println("nom Album Evenement");
                     ssc5 = sc.nextLine();
                     Event e;
                     if (!(mapevenement.containsKey(ssc5))){
                         e = new Event(ssc5);
                         mapevenement.put(e.getNom(),e);
                     } else {
                         e = mapevenement.get(ssc5);
                     }
                     AlbumPhotoEvent a1 = new AlbumPhotoEvent(e); 
                     listalbumev.get(ssc5).add(a1);
                     if(listalbumev.get(ssc5).size()>1){
                    	 a1.setNum(listalbumev.get(ssc5).size()-1);
                     }
                     break;
        }
    }
}