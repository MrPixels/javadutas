/**
 * Classe de gestion d'exception
 * @version 1.0
 */
public class PersonNotFoundException extends Exception{
	
	/**
	 * Construit un PersonNotFoundException
	 */
	public PersonNotFoundException() {
		System.out.println("personne non trouvé");
	}
}
