/**
 * Classe de gestion de Photo Evenementiel
 * @version 1.0
 */

import java.util.Date;
import java.util.StringTokenizer;

public class PhotoEvent extends Photo { 
	
	/**
	 * 	Evenement auquel la photo est lié
	 */
	private Event Evenement; 

	/**
	 * Appel au constructeur de Photo puis ajout de l'evenement
	 * @param chemin
	 * @param Evenement
	 * @throws Exception
	 */
	public PhotoEvent(String chemin,Event Evenement) throws Exception{
		super(chemin);
		if (this.BonEvent(Evenement)){
			this.Evenement = Evenement;
		}else throw new PhotoNotFoundException();
	}
	
	/**
	 * Verification de la correspondance entre le repertoire de la photo et celui donne
	 * @return false si la photo ne correspond pas a l'evenement donné sinon true si elle correspond
	 */
	private boolean BonEvent(Event Evenement){
		StringTokenizer st= new StringTokenizer(this.getPath(),"/");
		int token = st.countTokens();
		for (int j=0;j<token-1;j++){
			if(Evenement.getNom().equals(st.nextToken())) return true;
		}
		return false;
	}
	
	/**
	 * retourne l'evenement auquel est lié la photo
	 * @return Evenement
	 */ 
	public Event getEvent() {
		return this.Evenement;
	}
	
}	