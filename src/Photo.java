/**
 * Classe de gestion de Photo
 * @version 1.0 
 */
import java.util.Date;
import java.util.StringTokenizer;
import java.io.File;

public class Photo  {
	
	
	/**
	 * Nom du fichier dans le r�pertoire
	 */
	private String nom;
	/**
	 *  Date de creation de la photo
	 */
	private Date date;
	/**
	 * Chemin d'acces physique � la photo
	 */
	private String path;
	
	
	/**
	 * Verifie si le chemin est valide et creer la photo sinon renvoie une exception
	 * @param chemin
	 * @throws Exception
	 */
	public Photo(String chemin) throws PhotoNotFoundException,WrongExtensionException{
		File photo = new File(chemin);
		if (photo.exists()) {
			this.path = chemin;
			this.recuperationNom();
			this.recuperationDateFichier();
		} else throw new PhotoNotFoundException();
	}
	
	/**
	 * recupere la date de cr�ation de la photo
	 */
	private void recuperationDateFichier(){
		File fichier = new File(this.getNom());
		this.date= new Date(fichier.lastModified());
	}
		
	/**
	 * recupere le nom de la photo sans chemin ni extention
	 * @throws WrongExtensionException 
	 */
	private void recuperationNom() throws WrongExtensionException{
		StringTokenizer st= new StringTokenizer(this.getPath(),"/.");
		String[] extention = {"jpg", "jpeg", "bmp", "gif", "png"};
		String nomtmp;
		this.nom = "*@&&@*";
		int token = st.countTokens();
		for (int j=1;j<token-1;j++){
			st.nextToken();
		}
		nomtmp = st.nextToken();
		String ext = st.nextToken();
		for(String item: extention) {
			if(item.equals(ext)) this.nom = nomtmp;
		}
		if(this.nom=="*@&&@*"){
			throw new WrongExtensionException();
		}//TODO DEGUEU
	}
	
	/**
	 * retourne le nom de la photo
	 * @return nom photo
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * retourne chemin d'acces a la photo
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * retourne la date de derniere modification de la photo
	 * @return date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Retourne une String representant la photo
	 * @return s
	 */
	public String toString(){
		String s = new String(this.getPath());
		return s;
	}
}
