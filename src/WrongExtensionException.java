/**
 * Classe de gestion d'exception
 * @version 1.0
 */
public class WrongExtensionException extends Exception {

	/**
	 * Construit un WrongExtensionException
	 */
	public WrongExtensionException() {
		System.out.println("PIC'VIEW ne gere pas cette extension de fichier");
	}
}