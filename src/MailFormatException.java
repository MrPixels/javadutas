/**
 * Classe de gestion d'exception
 * @version 1.0
 */
public class MailFormatException extends Exception {
	/**
	 * Construit un MailFormatException
	 */
	public MailFormatException() {
		super("Le format du mail est mauvais.");
	}
}
