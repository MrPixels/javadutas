import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class TestProjetTXT {
	
	private static Map<String,Event> mapevenement = new HashMap<String,Event>();
	private static Map<String,ArrayList<AlbumPhotoEvent>> mapalbumev = new HashMap<String,ArrayList<AlbumPhotoEvent>>();
	private static Map<String,AlbumPhoto> mapalbum = new HashMap<String,AlbumPhoto>();
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) throws FileNotFoundException {
		String strNom;
		String strFic;
		int nalbev = 0;
		StringTokenizer st;
		String type;
		
		while(nalbev!=1 || nalbev!=3) {
			sc.nextLine();
			System.out.println("Bonjour, bienvenue sur le programme de creation et de visualisation d'albums photographiques PIC'VIEW.");
			System.out.println("Donnez le nom du fichier :");
			strFic = sc.nextLine();
			
			try {
				st = new StringTokenizer(strFic,"_.");
				type = st.nextToken();
				
				if(type.equals("Event")){
					strNom = st.nextToken();
					if(!(mapevenement.containsKey(strNom))){
						Event e = new Event(strFic,strNom);
						mapevenement.put(e.getNom(),e);
					} else {
						mapevenement.get(strNom).charger(strFic);
					}
				}
				else if(type.equals("Album")){
					strNom = st.nextToken();
					if(!(mapalbum.containsKey(strNom))){
						AlbumPhoto alb = new AlbumPhoto(strFic,strNom);
						mapalbum.put(alb.getNom(),alb);
					}else {
						mapalbum.get(strNom).charger(strFic);
					}
				}
				else if(type.equals("AlbumEvent")){
					strNom = st.nextToken();
					if(!(mapevenement.containsKey(strNom))) {
						Event e = new Event(strNom);
						mapevenement.put(e.getNom(),e);
					}
					if(!(mapalbumev.containsKey(strNom))){
						strNom = st.nextToken();
						AlbumPhotoEvent albev = new AlbumPhotoEvent(strFic,strNom,mapevenement.get(strNom));
						ArrayList<AlbumPhotoEvent> listalbev = new ArrayList<AlbumPhotoEvent>();
						listalbev.add(albev);
						mapalbumev.put(albev.getNom(),listalbev);
					}	
					else {
						strNom = st.nextToken();
						AlbumPhotoEvent albev = new AlbumPhotoEvent(strFic,strNom,mapevenement.get(strNom)); 
		                mapalbumev.get(strNom).add(albev);
		                if(mapalbumev.get(strNom).size()>1){
		                	albev.setNum(mapalbumev.get(strNom).size()-1);
		                }
					}
				}
				do {
					System.out.println("1 - quitter \n2 - sauvegarder \n3 - sauvegarder et quitter \n4 - continuer \n5 - Afficher Album");
					nalbev = sc.nextInt();
					switch (nalbev){
						case 1: break;
						case 2: sauvFichier(); break;
						case 3: sauvFichier(); break;
						case 4: break;
						case 5: Affichage(); break;
					}
				}while(nalbev<= 0 || nalbev > 5);
			}catch(NoSuchElementException e) { System.out.println(e);
			}catch(Exception e) { System.out.println(e);}
		}
		sc.close();
	}
	
	
	
	private static void sauvFichier() {
		for(Event currentValue : mapevenement.values()){
			currentValue.sauvegarde();
		}
		for(AlbumPhoto currentValue : mapalbum.values()){
			currentValue.sauvegarde();
		}
		for(ArrayList<AlbumPhotoEvent> currentValue : mapalbumev.values()){
			for(AlbumPhotoEvent value : currentValue) {
				value.sauvegarde();
			}
		}
	}
	
	private static void Affichage() {
		for(AlbumPhoto currentValue : mapalbum.values()){
			System.out.println(currentValue.toString());
			System.out.println();
		}
		for(ArrayList<AlbumPhotoEvent> currentValue : mapalbumev.values()){
			for(AlbumPhotoEvent value : currentValue) {
				System.out.println(value.toString());
				System.out.println();
			}
		}
	}
}


